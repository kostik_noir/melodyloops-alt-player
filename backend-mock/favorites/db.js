const data = {
  playlists: []
};

module.exports.getAllPlaylists = () => data.playlists;

module.exports.addTrackToPlaylist = (playlistTitle, trackId) => {
  if (!hasPlaylistWithTitle(playlistTitle)) {
    createPlaylist(playlistTitle);
  }

  const playlist = getPlaylistByTitle(playlistTitle);
  if (playlist.tracks.indexOf(trackId) !== -1) {
    return;
  }
  playlist.tracks.push(trackId);
};

module.exports.removeTrackFromPlaylist = (playlistTitle, trackId) => {
  if (!hasPlaylistWithTitle(playlistTitle)) {
    return;
  }

  const playlist = getPlaylistByTitle(playlistTitle);
  playlist.tracks = playlist.tracks.filter(id => id !== trackId);
};

module.exports.isTrackFavorite = (trackId) => {
  for (const playlist of data.playlists) { // eslint-disable-line no-restricted-syntax
    if (playlist.tracks.includes(trackId)) {
      return true;
    }
  }
  return false;
};

function getPlaylistByTitle(playlistTitle) {
  return data.playlists.filter(({ title }) => title === playlistTitle)[0] || null;
}

function hasPlaylistWithTitle(playlistTitle) {
  return getPlaylistByTitle(playlistTitle) !== null;
}

function createPlaylist(playlistTitle) {
  data.playlists.push({
    title: playlistTitle,
    tracks: []
  });
}
