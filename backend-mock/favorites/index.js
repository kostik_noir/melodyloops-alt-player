const express = require('express');
const db = require('./db');

const router = new express.Router();

router.get('/', (req, res) => {
  setTimeout(() => {
    res.send(db.getAllPlaylists());
  }, 1000);
});

router.post('/', (req, res) => {
  const { trackId, playlistTitle } = req.body;
  db.addTrackToPlaylist(playlistTitle, trackId);
  res.send('');
});

router.delete('/', (req, res) => {
  const { trackId, playlistTitle } = req.body;
  db.removeTrackFromPlaylist(playlistTitle, trackId);
  res.json({
    isFavorite: db.isTrackFavorite(trackId)
  });
});

module.exports = router;
