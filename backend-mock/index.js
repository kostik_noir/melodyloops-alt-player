require('dotenv').config();
const os = require('os');
const express = require('express');
const cors = require('cors');
const compression = require('compression');

const playlistsRouter = require('./playlists');
const favoritesRouter = require('./favorites');

const host = process.env.API_SERVER_HOST.replace(/\/*$/, '') || (os.platform() === 'linux' ? '0.0.0.0' : '127.0.0.1');
const port = process.env.API_SERVER_PORT || 3001;
const root = (process.env.API_SERVER_ROOT || 'api').replace(/^\/*/, '').replace(/\/*$/, '');

const app = express();
app.use(cors());
app.use(express.json());
app.use(compression());

app.use(`/${root}/playlists`, playlistsRouter);
app.use(`/${root}/favorites`, favoritesRouter);

app.listen(port, host, () => {
  console.log(`API server is running at http://${host}:${port}`);
});
