const express = require('express');
const axios = require('axios');

const urlPrefix = 'https://www.melodyloops.com/get/xml/music';

const router = new express.Router();
module.exports = router;

router.get('/:playlistId', (req, res) => {
  const { playlistId } = req.params;
  const url = `${urlPrefix}/${playlistId}/`;

  axios.get(url)
    .catch((err) => {
      res.status(500);
      res.send(err.message);
    })
    .then((resp) => {
      res.set('Content-Type', 'text/xml');
      res.send(resp.data);
    });
});
