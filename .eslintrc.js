module.exports = {
  root: true,
  extends: [
    'airbnb-base',
    'plugin:react/recommended'
  ],
  plugins: [],
  env: {
    browser: true,
    node: true,
    // es6: true
  },
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  settings: {
    'import/resolver': {
      node: {}, // https://github.com/benmosher/eslint-plugin-import/issues/1396
      webpack: {
        config: './webpack.common.js'
      }
    },
    react: {
      pragma: 'h'
    }
  },
  rules: {
    'comma-dangle': ['error', 'never'],
    'no-param-reassign': 'off',
    'no-use-before-define': ['error', 'nofunc'],
    'no-plusplus': 'off',
    'func-names': 'off',
    'default-case': 'off',
    'no-shadow': 'off',
    'vars-on-top': 'off',
    'max-len': ['error', 120],
    'import/imports-first': 'off',
    'import/prefer-default-export': 'off',
    'curly': ['error', 'all'],
    'brace-style': ['error', '1tbs', { allowSingleLine: false }],
    'no-mixed-operators': ['off'],
    'arrow-parens': ['error', 'as-needed', { requireForBlockBody: true }],
    'no-empty': ['error', { allowEmptyCatch: true }],
    'object-curly-newline': ['off'],
    'prefer-destructuring': ['error', {
      array: false
    }],

    // these settings was copy/pasted from node_modules/eslint-config-airbnb-base/rules/style.js
    // The field MemberExpression was modified
    indent: ['error', 2, {
      SwitchCase: 1,
      VariableDeclarator: 1,
      outerIIFEBody: 1,
      MemberExpression: 1,
      FunctionDeclaration: {
        parameters: 1,
        body: 1
      },
      FunctionExpression: {
        parameters: 1,
        body: 1
      }
    }],

    'react/display-name': ['off'],
    'react/prop-types': ['off'] // TODO remove this rule
  }
};
