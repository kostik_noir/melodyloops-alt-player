const MOUSE_INTERACTION_TYPE = 1;
const TOUCH_INTERACTION_TYPE = 2;

export default function create({ el, onDragStart, onDrag, onDragEnd }) {
  let isActve = true;

  const ctx = {
    el,
    listeners: {
      onDragStart,
      onDrag,
      onDragEnd
    },
    state: null
  };
  stateReady(ctx);

  return {
    destroy
  };

  function destroy() {
    if (!isActve) {
      return;
    }
    isActve = false;

    ctx.state.exit();
  }
}

function stateReady(ctx) {
  ctx.state = { exit };

  const { el } = ctx;
  el.addEventListener('mousedown', onDragStarted, false);
  el.addEventListener('touchstart', onDragStarted, false);

  function exit() {
    el.removeEventListener('mousedown', onDragStarted, false);
    el.removeEventListener('touchstart', onDragStarted, false);
  }

  function onDragStarted(event) {
    event.preventDefault();

    let interactionType;
    switch (event.type) {
      case 'mousedown':
        interactionType = MOUSE_INTERACTION_TYPE;
        break;
      case 'touchstart':
        interactionType = TOUCH_INTERACTION_TYPE;
        break;
    }

    exit();

    notifyOnDragStartListener(calcHorizontalPosition(event.pageX, el));
    ctx.interactionType = interactionType;
    stateDragging(ctx);
  }

  function notifyOnDragStartListener(v) {
    if (typeof ctx.listeners.onDragStart === 'function') {
      ctx.listeners.onDragStart(v);
    }
  }
}

function stateDragging(ctx) {
  let isActive = true;
  let isNotifyOnDragScheduled = false;
  let position;

  ctx.state = { exit };

  const { el, interactionType } = ctx;

  switch (interactionType) {
    case MOUSE_INTERACTION_TYPE:
      document.addEventListener('mousemove', onMove, false);
      document.addEventListener('mouseup', onEnd, false);
      break;
    case TOUCH_INTERACTION_TYPE:
      document.addEventListener('touchmove', onMove, false);
      document.addEventListener('touchend', onEnd, false);
      break;
  }

  function exit() {
    isActive = false;

    document.removeEventListener('mousemove', onMove, false);
    document.removeEventListener('mouseup', onEnd, false);
    document.removeEventListener('touchmove', onMove, false);
    document.removeEventListener('touchend', onEnd, false);

    stateReady(ctx);
  }

  function onMove(event) {
    event.preventDefault();

    position = calcHorizontalPosition(event.pageX, el);
    notifyOnDragListener();
  }

  function onEnd(event) {
    event.preventDefault();

    position = calcHorizontalPosition(event.pageX, el);
    notifyOnDragEndListener();
    exit();
  }

  function notifyOnDragEndListener() {
    if (typeof ctx.listeners.onDragEnd === 'function') {
      ctx.listeners.onDragEnd(position);
    }
  }

  function notifyOnDragListener() {
    if (typeof ctx.listeners.onDrag !== 'function' || isNotifyOnDragScheduled) {
      return;
    }
    isNotifyOnDragScheduled = true;

    requestAnimationFrame(() => {
      isNotifyOnDragScheduled = false;

      if (isActive) {
        ctx.listeners.onDrag(position);
      }
    });
  }
}

function calcHorizontalPosition(pointerX, el) {
  const { left, width } = el.getBoundingClientRect();
  const elX = left + window.pageXOffset;

  return Math.min(1, Math.max(0, (pointerX - elX) / width));
}
