export function parse(node) {
  const result = {
    playlists: []
  };

  node.querySelectorAll('[data-playlists] [data-playlist]').forEach((el) => {
    result.playlists.push({
      url: el.getAttribute('data-url'),
      title: el.getAttribute('data-title') || '',
      isActive: el.getAttribute('data-is-active') === 'true',
      withFreeTracks: el.getAttribute('data-with-free-tracks') === 'true',
      withDownloadableTracks: el.getAttribute('data-with-downloadable-tracks') === 'true'
    });
  });

  return result;
}
