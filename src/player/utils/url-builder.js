export function getWaveFormImgUrl(trackFileName, duration, isDefaultVariation, isUnlooped) {
  const pattern = 'https://cdn2.melodyloops.com/i/wav/{IMG_FILE_NAME}.png';

  const createUrl = imgFileName => pattern.replace(/\{IMG_FILE_NAME\}/gi, imgFileName);

  if (isDefaultVariation) {
    return createUrl(trackFileName);
  }

  if (isUnlooped) {
    return createUrl(`${trackFileName}-2${duration}`);
  }

  return createUrl(`${trackFileName}-1${duration}`);
}

export function getTrackPageUrl(trackFileName) {
  return `https://www.melodyloops.com/tracks/${trackFileName}/`;
}

export function getComposerPageUrl(composerId) {
  return `https://www.melodyloops.com/composers/${composerId}/`;
}

export function getLongoloopsPageUrl(trackFileName) {
  return `https://www.melodyloops.com/my-music/longoloops/${trackFileName}/`;
}

export function getCheckoutLinkUrl(ids) {
  return `https://www.melodyloops.com/order/checkout/?id=${ids.join('+')}`;
}
