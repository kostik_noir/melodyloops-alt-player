// TODO save the data in cookies

const keyPrefix = '__player';

export function setItem(key, value) {
  localStorage.setItem(`${keyPrefix}.${key}`, value);
}

export function getItem(key) {
  return localStorage.getItem(`${keyPrefix}.${key}`);
}
