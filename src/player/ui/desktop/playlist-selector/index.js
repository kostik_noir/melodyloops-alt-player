import { h } from 'preact';
import { connect } from 'react-redux';
import { getPlaylists } from '../../../state/playlists';
import * as selectedPlaylist from '../../../state/selected-playlist';
import * as styles from './styles.scss';

const mapStateToProps = state => ({
  items: getPlaylists(state),
  selectedItemUrl: selectedPlaylist.getUrl(state)
});

const mapDispatchToProps = dispatch => ({
  onItemSelected: (playlistUrl) => {
    dispatch(selectedPlaylist.change(playlistUrl));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Selector);

function Selector({ items, selectedItemUrl, onItemSelected }) {
  if (items.length < 2) {
    return null;
  }

  return (
    <div className={ styles.root }>
      {
        items.map(obj => (
          <Item
            key={ obj.url }
            url={ obj.url }
            title={ obj.title } i
            isSelected={ obj.url === selectedItemUrl }
            onClick={ onItemSelected }
          />
        ))
      }
    </div>
  );
}

function Item({ url, title, isSelected, onClick }) {
  const onClickHandler = () => {
    onClick(url);
  };

  let className = [styles.item];
  if (isSelected) {
    className.push(styles.isSelected);
  }
  className = className.join(' ');

  return (
    <div className={ className } onClick={ onClickHandler }>{ title }</div>
  );
}
