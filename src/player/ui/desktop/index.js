import { h } from 'preact';
import PlaylistSelector from './playlist-selector';
import Body from './body';
import * as styles from './styles.scss';

export default function DesktopUI() {
  return (
    <div className={ styles.root }>
      <PlaylistSelector/>
      <Body/>
    </div>
  );
}
