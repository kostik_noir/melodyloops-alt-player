import { h } from 'preact';
import { connect } from 'react-redux';
import * as selectedPlaylist from '../../../state/selected-playlist';
import ScreenLoading from '../screen-loading';
import ScreenError from '../screen-error';
import ScreenReady from '../screen-ready';
import * as styles from './styles.scss';

const mapStateToProps = state => ({
  isLoading: selectedPlaylist.isLoadingStarted(state),
  isReady: selectedPlaylist.isLoadingCompleted(state),
  isFailed: selectedPlaylist.isLoadingFailed(state)
});

export default connect(mapStateToProps)(PlayerBody);

function PlayerBody({ isLoading, isReady, isFailed }) {
  let className = [styles.root];
  let child;

  switch (true) {
    case isLoading:
      className.push(styles.stateLoading);
      child = <ScreenLoading/>;
      break;
    case isReady:
      className.push(styles.stateReady);
      child = <ScreenReady/>;
      break;
    case isFailed:
      className.push(styles.stateError);
      child = <ScreenError/>;
      break;
    default:
      child = null;
  }
  className = className.join(' ');

  return (
    <div className={ className }>
      { child }
    </div>
  );
}
