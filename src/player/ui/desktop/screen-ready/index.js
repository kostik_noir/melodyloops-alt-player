import { h } from 'preact';
import { SelectedPlaylist, CartPlaylist } from '../widgets/playlist';
import { TrackLink, ComposerLink } from '../widgets/track-links';
import PlaybackProgressBar from '../widgets/playback-progress-bar';
import ControlPanel from '../widgets/control-panel';
import CartExtraUI from '../widgets/cart-extra-ui';
import * as styles from './styles.scss';

export default function ScreenReady() {
  return (
    <div className={ styles.root }>
      <div className={ styles.section }>
        <SelectedPlaylist/>
      </div>
      <div className={ styles.section }>
        <TrackLink className={ styles.link }/>
        <ComposerLink className={ styles.link }/>
        <PlaybackProgressBar className={ styles.progressBar }/>
        <ControlPanel className={ styles.bottomControlPanel }/>
      </div>
      <div className={ styles.section }>
        <div className={ styles.cartPlaylistWrapper }>
          <CartPlaylist/>
        </div>
        <CartExtraUI className={ styles.cartExtraUi }/>
      </div>
    </div>
  );
}
