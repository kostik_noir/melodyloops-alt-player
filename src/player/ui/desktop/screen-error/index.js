import { h } from 'preact';
import { connect } from 'react-redux';
import * as selectedPlaylist from '../../../state/selected-playlist';
import * as styles from './styles.scss';

const mapDispatchToProps = dispatch => ({
  requestReload: () => {
    dispatch(selectedPlaylist.reload());
  }
});

export default connect(null, mapDispatchToProps)(ScreenError);

function ScreenError({ requestReload }) {
  return (
    <div className={ styles.root }>
      <div className={ styles.icon }></div>
      <div className={ styles.title }>can&apos;t load playlist</div>
      <div className={ styles.btn } onClick={ requestReload }>try again</div>
    </div>
  );
}
