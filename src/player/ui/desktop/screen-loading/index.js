import { h } from 'preact';
import PlaylistPreloader from './playlist-preloader';
import * as styles from './styles.scss';

export default function ScreenLoading() {
  return (
    <div className={ styles.root }>
      <PlaylistPreloader/>
    </div>
  );
}
