import { h } from 'preact';
import Spinner from '../../../../../common/ui/spinner/index';
import * as styles from './styles.scss';

export default function PlaylistPreloader() {
  return (
    <div className={ styles.root }>
      <Spinner/>
      <div className={ styles.title }>loading playlist</div>
    </div>
  );
}
