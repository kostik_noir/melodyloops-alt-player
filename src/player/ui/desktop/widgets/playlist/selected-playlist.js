import { h } from 'preact';
import { connect } from 'react-redux';
import * as selectedPlaylist from '../../../../state/selected-playlist';
import * as activeTrack from '../../../../state/active-track';
import BasePlaylist from './base';

const mapStateToProps = state => ({
  tracks: selectedPlaylist.getTracks(state)
});

const mapDispatchToProps = dispatch => ({
  onTrackPressed: (id) => {
    dispatch(activeTrack.changeTrack({
      trackId: id,
      fromSelectedPlaylist: true
    }));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectedPlaylist);

function SelectedPlaylist({ tracks, onTrackPressed }) {
  return (
    <BasePlaylist tracks={ tracks } onTrackPressed={ onTrackPressed }/>
  );
}
