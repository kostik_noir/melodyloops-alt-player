import { h } from 'preact';
import { useEffect, useRef } from 'preact/hooks';
import { connect } from 'react-redux';
import IScroll from 'iscroll';
import * as activeTrack from '../../../../state/active-track';
import styles from './styles.scss';
import { formatDuration } from '../../../../utils/time';

const mapStateToProps = state => ({
  activeTrackId: activeTrack.getData(state).id
});

export default connect(mapStateToProps)(BasePlaylist);

function BasePlaylist({ tracks, onTrackPressed, activeTrackId }) {
  const scroller = useRef(null);
  const scrollerRoot = useRef(null);

  useEffect(() => {
    if (scroller.current === null) {
      scroller.current = new IScroll(scrollerRoot.current, {
        mouseWheel: true,
        tap: true,
        scrollbars: true,
        interactiveScrollbars: true,
        momentum: false
      });

      setTimeout(() => {
        scrollerRoot.current.classList.add(styles.isReady);
      }, 0);
    }

    return () => {
      scroller.current.destroy();
    };
  }, []);

  useEffect(() => {
    scroller.current.refresh();
    updateScrollbarVisibility(scroller.current, scrollerRoot.current);
    updateScrollPosition(scroller.current, scrollerRoot.current, activeTrackId);
  }, [activeTrackId, tracks]);

  return (
    <div className={ styles.root } ref={ scrollerRoot }>
      <div className={ styles.scrollWrapper }>
        <div className={ styles.tracks }>
          {
            tracks.map((obj, index) => (
              <Item
                key={ obj.id }
                index={ index + 1 }
                id={ obj.id }
                title={ obj.title }
                duration={ getDefaultTrackDuration(obj) }
                isSelected={ obj.id === activeTrackId }
                onPress={ onTrackPressed }
              />
            ))
          }
        </div>
      </div>
    </div>
  );
}

function Item({ index, id, title, duration, isSelected, onPress }) {
  const onClick = () => {
    onPress(id);
  };

  const formatIndex = (index) => {
    if (index > 9) {
      return `${index}`;
    }
    return `0${index}`;
  };

  let className = [styles.track];
  if (isSelected) {
    className.push(styles.isSelected);
  }
  className = className.join(' ');

  return (
    <div data-id={ id } className={ className } onClick={ onClick }
         data-index={ formatIndex(index) }
         data-duration={ formatDuration(duration) }>
      <div className={ styles.trackTitle }>{ title }</div>
    </div>
  );
}

function getDefaultTrackDuration(track) {
  const { variations } = track;

  const defaultVariation = variations.filter(obj => obj.isDefault === true)[0];
  if (typeof defaultVariation !== 'undefined') {
    return defaultVariation.duration;
  }

  if (variations.length > 0) {
    return variations[0].duration;
  }

  return 0;
}

function updateScrollbarVisibility(scroller, rootEl) {
  if (scroller.hasVerticalScroll) {
    rootEl.classList.remove(styles.withoutVerticalScrollbar);
  } else {
    rootEl.classList.add(styles.withoutVerticalScrollbar);
  }
}

function updateScrollPosition(scroller, rootNode, activeTrackId) {
  const el = rootNode.querySelector(`[data-id="${activeTrackId}"]`);
  if (el === null) {
    return;
  }

  const rootNodeBox = getNodeBox(rootNode);
  const elBox = getNodeBox(el);

  if ((elBox.top > rootNodeBox.top) && (elBox.top + elBox.height) < (rootNodeBox.top + rootNodeBox.height)) {
    return;
  }

  scroller.scrollToElement(el, 500, null, true);
}

function getNodeBox(node) {
  const { left, top, width, height } = node.getBoundingClientRect();
  return {
    left: left + window.pageXOffset,
    top: top + window.pageYOffset,
    width,
    height
  };
}
