import { h } from 'preact';
import { connect } from 'react-redux';
import * as cart from '../../../../state/cart';
import * as activeTrack from '../../../../state/active-track';
import BasePlaylist from './base';

const mapStateToProps = state => ({
  tracks: cart.getTracks(state)
});

const mapDispatchToProps = dispatch => ({
  onTrackPressed: (id) => {
    dispatch(activeTrack.changeTrack({
      trackId: id,
      fromCartPlaylist: true
    }));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CartPlaylist);

function CartPlaylist({ tracks, onTrackPressed }) {
  return (
    <BasePlaylist tracks={ tracks } onTrackPressed={ onTrackPressed }/>
  );
}
