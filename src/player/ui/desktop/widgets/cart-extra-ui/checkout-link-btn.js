import { h } from 'preact';
import { connect } from 'react-redux';
import * as cart from '../../../../state/cart';
import * as urlBuilder from '../../../../utils/url-builder';
import styles from './styles.scss';

const mapStateToProps = state => ({
  ids: cart.getTracks(state).map(obj => obj.id)
});

export default connect(mapStateToProps)(CheckoutLinkBtn);

function CheckoutLinkBtn({ ids }) {
  if (ids.length === 0) {
    return null;
  }

  return (
    <a className={ styles.checkoutLink } href={ urlBuilder.getCheckoutLinkUrl(ids) }>Proceed to Checkout</a>
  );
}
