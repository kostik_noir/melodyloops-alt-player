import { h } from 'preact';
import ItemsCounter from './items-counter';
import CheckoutLinkBtn from './checkout-link-btn';
import styles from './styles.scss';

export default function CartExtraUI({ className: extraClassName }) {
  let className = [styles.root];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  return (
    <div className={ className }>
      <ItemsCounter/>
      <CheckoutLinkBtn/>
    </div>
  );
}
