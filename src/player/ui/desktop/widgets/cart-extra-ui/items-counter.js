import { h } from 'preact';
import { connect } from 'react-redux';
import * as cart from '../../../../state/cart';
import styles from './styles.scss';

const mapStateToProps = state => ({
  count: cart.getTracks(state).length
});

export default connect(mapStateToProps)(ItemsCounter);

function ItemsCounter({ count }) {
  if (count === 0) {
    return null;
  }

  return (
    <div className={ styles.itemsCounter }>
      <span className={ styles.itemsCounterTitle }>Shopping Cart:</span>
      <span className={ styles.itemsCounterValue }>{ count }</span>
    </div>
  );
}
