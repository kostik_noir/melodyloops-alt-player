import { h } from 'preact';
import { connect } from 'react-redux';
import { formatDuration } from '../../../../../utils/time';
import * as activeTrack from '../../../../../state/active-track';
import styles from './styles.scss';

const mapStateToProps = (state) => {
  const { variations, selectedVariationIndex } = activeTrack.getData(state);
  return {
    variations,
    selectedVariationIndex
  };
};

const mapDispatchToProps = dispatch => ({
  onItemPressed: (variationIndex) => {
    dispatch(activeTrack.changeVariation(variationIndex));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(VariationList);

function VariationList({ variations, selectedVariationIndex, onItemPressed }) {
  if (variations.length < 2) {
    return null;
  }

  const defaultVariation = variations.filter(obj => obj.isDefault)[0];
  const otherVariations = variations.filter(obj => obj !== defaultVariation);

  const variationsForRendering = [];
  if (typeof defaultVariation !== 'undefined') {
    variationsForRendering.push({
      ...defaultVariation,
      originalIndex: variations.indexOf(defaultVariation)
    });
  }
  otherVariations.forEach((obj) => {
    variationsForRendering.push({
      ...obj,
      originalIndex: variations.indexOf(obj)
    });
  });

  return (
    <div className={ styles.root }>
      {
        variationsForRendering.map(obj => (
          <Item
            key={ obj.originalIndex }
            duration={ obj.duration }
            index={ obj.originalIndex }
            isSelected={ obj.originalIndex === selectedVariationIndex }
            onClick={ onItemPressed }
          />
        ))
      }
    </div>
  );
}

function Item({ duration, index, isSelected, onClick }) {
  const handleClick = () => {
    if (isSelected) {
      return;
    }
    onClick(index);
  };

  let className = [styles.item];
  if (isSelected) {
    className.push(styles.isSelected);
  }
  className = className.join(' ');

  return (
    <div className={ className } onClick={ handleClick }>{ formatDuration(duration) }</div>
  );
}
