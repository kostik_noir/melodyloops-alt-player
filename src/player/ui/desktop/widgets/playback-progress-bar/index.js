import { h } from 'preact';
import { connect } from 'react-redux';
import VariationList from './variation-list';
import { CurrentTimeViewer, TrackDurationViewer } from './time-viewers';
import ProgressBar from './progress-bar';
import styles from './styles.scss';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(PlaybackProgressBar);

function PlaybackProgressBar({ className: extraClassName }) {
  let className = [styles.root];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  return (
    <div className={ className }>
      <div className={ styles.topSection }>
        <CurrentTimeViewer className={ styles.timeViewer }/>
        <VariationList/>
        <TrackDurationViewer className={ styles.timeViewer }/>
      </div>
      <ProgressBar className={ styles.progressBar }/>
    </div>
  );
}
