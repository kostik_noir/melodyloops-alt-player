import { h } from 'preact';
import { useEffect, useState, useRef } from 'preact/hooks';
import { connect } from 'react-redux';
import * as activeTrack from '../../../../../state/active-track';
import createDragArea from '../../../../../utils/drag-area';
import * as urlBuilder from '../../../../../utils/url-builder';
import * as styles from './styles.scss';

const mapStateToProps = (state) => {
  const { fileName, selectedVariationIndex, variations } = activeTrack.getData(state);
  const { isUnlooped, duration, isDefault } = variations[selectedVariationIndex];

  return {
    imgUrl: urlBuilder.getWaveFormImgUrl(fileName, duration, isDefault, isUnlooped)
  };
};

export default connect(mapStateToProps)(ProgressBar);

function ProgressBar({ imgUrl, className: extraClassName }) {
  let className = [styles.root];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  const ref = useRef(null);

  const [playbackRatio, setPlaybackRatio] = useState(0);
  useEffect(() => {
    const dragArea = createDragArea({
      el: ref.current,
      onDragStart: setPlaybackRatio,
      onDrag: setPlaybackRatio,
      onDragEnd: setPlaybackRatio
    });

    return () => {
      dragArea.destroy();
    };
  }, []);

  const playbackProgressElStyles = {
    width: `${playbackRatio * 100}%`
  };

  return (
    <div className={ className } ref={ ref }>
      <img className={ styles.img } src={ imgUrl }/>
      <div className={ styles.imgBg }></div>
      <div className={ styles.loadingProgress }></div>
      <div className={ styles.playbackProgress } style={ playbackProgressElStyles }></div>
    </div>
  );
}
