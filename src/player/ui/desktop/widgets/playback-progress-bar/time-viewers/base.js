import { h } from 'preact';
import { formatDuration } from '../../../../../utils/time';
import * as styles from './styles.scss';

export default function ({ time, className: extraClassName }) {
  let className = [styles.root];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  return (
    <div className={ className }>{ formatDuration(time) }</div>
  );
}
