import { h } from 'preact';
import { connect } from 'react-redux';
import * as activeTrack from '../../../../../state/active-track';
import BaseTimeViewer from './base';

const mapStateToProps = (state) => {
  const { variations, selectedVariationIndex } = activeTrack.getData(state);
  return {
    duration: variations[selectedVariationIndex].duration
  };
};

export default connect(mapStateToProps)(TrackDurationViewer);

function TrackDurationViewer({ duration, ...props }) {
  return (
    <BaseTimeViewer time={ duration } { ...props }/>
  );
}
