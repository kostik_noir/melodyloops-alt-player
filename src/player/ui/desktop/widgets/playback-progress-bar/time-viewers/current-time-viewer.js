import { h } from 'preact';
import BaseTimeViewer from './base';

// TODO connect with audio player
export default function CurrentTimeViewer({ ...props }) {
  return (
    <BaseTimeViewer time={ 0 } { ...props } />
  );
}
