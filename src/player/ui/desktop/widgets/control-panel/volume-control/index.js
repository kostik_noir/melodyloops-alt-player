import { h } from 'preact';
import { useState, useRef, useEffect } from 'preact/hooks';
import * as styles from './styles.scss';
import createDragArea from '../../../../../utils/drag-area';

// TODO save volume level in local storage

export default function VolumeControl() {
  const volumeLevelBar = useRef(null);

  const [volumeRatio, setVolumeRatio] = useState(0);

  useEffect(() => {
    const dragArea = createDragArea({
      el: volumeLevelBar.current,
      onDragStart: setVolumeRatio,
      onDrag: setVolumeRatio,
      onDragEnd: setVolumeRatio
    });

    return () => {
      dragArea.destroy();
    };
  }, []);

  const [isMuted, changeMutedState] = useState(false);

  const handleClickOnMuteBtn = () => {
    changeMutedState(!isMuted);
  };

  let muteBtnClassName = [styles.btn];
  if (isMuted) {
    muteBtnClassName.push(styles.isMuted);
  }
  muteBtnClassName = muteBtnClassName.join(' ');

  let maskWidth = volumeRatio * 100;
  if (isMuted) {
    maskWidth = 0;
  }
  const maskStyles = {
    width: `${maskWidth}%`
  };

  return (
    <div className={ styles.root }>
      <div className={ muteBtnClassName } onClick={ handleClickOnMuteBtn }></div>
      <div className={ styles.volumeLevelBar } ref={ volumeLevelBar }>
        <div className={ styles.volumeLevelDots }>
          <div className={ styles.volumeLevelDot }></div>
          <div className={ styles.volumeLevelDot }></div>
          <div className={ styles.volumeLevelDot }></div>
          <div className={ styles.volumeLevelDot }></div>
          <div className={ styles.volumeLevelDot }></div>
        </div>
        <div className={ styles.volumeLevelMask } style={ maskStyles }>
          <div className={ styles.volumeLevelDots }>
            <div className={ styles.volumeLevelDot }></div>
            <div className={ styles.volumeLevelDot }></div>
            <div className={ styles.volumeLevelDot }></div>
            <div className={ styles.volumeLevelDot }></div>
            <div className={ styles.volumeLevelDot }></div>
          </div>
        </div>
      </div>
    </div>
  );
}
