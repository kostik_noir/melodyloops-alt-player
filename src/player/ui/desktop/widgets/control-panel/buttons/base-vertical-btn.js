import { h } from 'preact';
import * as styles from './styles/vertical-btn.scss';

export default function BaseVerticalBtn({ title = '', className: extraClassName, ...props }) {
  let className = [styles.root];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  return (
    <div className={ className } { ...props }>
      <div className={ styles.bg }>
        <div className={ styles.icon }></div>
      </div>
      <div className={ styles.title }>{ title }</div>
    </div>
  );
}
