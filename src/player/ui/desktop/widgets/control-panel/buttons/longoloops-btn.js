import { h } from 'preact';
import { connect } from 'react-redux';
import * as activeTrack from '../../../../../state/active-track';
import * as urlBuilder from '../../../../../utils/url-builder';
import * as styles from './styles/vertical-btn.scss';

const mapStateToProps = (state) => {
  const track = activeTrack.getData(state);
  return {
    link: urlBuilder.getLongoloopsPageUrl(track.fileName)
  };
};

export default connect(mapStateToProps)(LongoloopsBtn);

function LongoloopsBtn({ link, className: extraClassName }) {
  let className = [styles.root, styles.longoloopsBtn];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  return (
    <a href={ link } className={ className }>
      <div className={ styles.bg }>
        <div className={ styles.icon }></div>
      </div>
      <div className={ styles.title }>{ 'longoloops' }</div>
    </a>
  );
}
