import { h } from 'preact';
import { forwardRef } from 'preact/compat';
import * as styles from './styles/horizontal-btn.scss';

export default forwardRef((props, ref) => {
  const { title = '', className: extraClassName, ...extraProps } = props;
  let className = [styles.root];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  return (
    <div className={ className } ref={ ref } { ...extraProps }>
      <div className={ styles.icon }></div>
      <div className={ styles.title }>{ title }</div>
    </div>
  );
});
