import { h } from 'preact';
import { connect } from 'react-redux';
import * as activeTrack from '../../../../../state/active-track';
import BaseBtn from './base-vertical-btn';
import * as styles from './styles/vertical-btn.scss';

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(activeTrack.selectPreviousTrack());
  }
});

export default connect(null, mapDispatchToProps)(PrevBtn);

function PrevBtn({ onClick }) {
  return (
    <BaseBtn className={ styles.prevBtn } title={ 'prev' } onClick={ onClick }/>
  );
}
