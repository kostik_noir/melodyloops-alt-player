import { h } from 'preact';
import { connect } from 'react-redux';
import * as activeTrack from '../../../../../../../state/active-track';
import * as urlBuilder from '../../../../../../../utils/url-builder';
import FbBtn from './buttons/fb';
import TwitterBtn from './buttons/twitter';
import CopyToClipboardBtn from './buttons/copy-to-clipboard';
import styles from './styles.scss';

const mapStateToProps = (state) => {
  const track = activeTrack.getData(state);
  return {
    trackUrl: urlBuilder.getTrackPageUrl(track.fileName)
  };
};

export default connect(mapStateToProps)(ShareBtnTooltip);

function ShareBtnTooltip({ trackUrl }) {
  return (
    <div className={ styles.root }>
      <div className={ styles.body }>
        <FbBtn trackUrl={ trackUrl }/>
        <TwitterBtn trackUrl={ trackUrl }/>
        <CopyToClipboardBtn trackUrl={ trackUrl }/>
      </div>
    </div>
  );
}
