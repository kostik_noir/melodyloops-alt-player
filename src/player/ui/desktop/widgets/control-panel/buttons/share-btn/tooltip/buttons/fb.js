import { h } from 'preact';
import styles from '../styles.scss';

export default function ShareWithFacebookBtn({ trackUrl }) {
  const className = [styles.btn, styles.facebookBtn].join(' ');

  const url = encodeURI(trackUrl);

  const handleClick = () => {
    window.open(
      `https://www.facebook.com/sharer/sharer.php?u=${url}`,
      '__test',
      'resizable'
    );
  };

  return (
    <div className={ className } onClick={ handleClick }></div>
  );
}
