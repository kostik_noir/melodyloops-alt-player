import { h } from 'preact';
import styles from '../styles.scss';

export default function ShareWithTwitterBtn({ trackUrl }) {
  const className = [styles.btn, styles.twitterBtn].join(' ');

  const url = encodeURI(trackUrl);

  const handleClick = () => {
    window.open(
      `https://twitter.com/intent/tweet?url=${url}`,
      '__test', // TODO
      'resizable'
    );
  };

  return (
    <div className={ className } onClick={ handleClick }></div>
  );
}
