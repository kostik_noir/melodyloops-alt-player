import { h } from 'preact';
import { useRef, useState, useEffect } from 'preact/hooks';
import styles from '../styles.scss';

export default function CopyToClipboardBtn({ trackUrl }) {
  const className = [styles.btn, styles.copyToClipboardBtn].join(' ');

  const ref = useRef(null);
  const [animation] = useState(createAnimation(ref));
  useEffect(() => () => {
    animation.destroy();
  });

  const handleClick = () => {
    copyToClipboard(trackUrl);
    animation.start();
  };

  return (
    <div className={ className } onClick={ handleClick } ref={ ref }></div>
  );
}

function copyToClipboard(v) {
  const el = document.createElement('textarea');
  el.value = v;
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
}

function createAnimation(ref) {
  let isStarted = false;
  let rootEl = null;
  let el = null;
  let timeoutId = null;

  return {
    start,
    destroy
  };

  function start() {
    if (isStarted) {
      return;
    }
    isStarted = true;

    if (rootEl === null) {
      rootEl = ref.current;
    }

    createAnimationEl();

    timeoutId = setTimeout(() => {
      timeoutId = null;
      el.classList.add(styles.isActive);
    }, 0);
  }

  function destroy() {
    clearTimeout(timeoutId);
    destroyAnimationEl();
  }

  function onTransitionEnd() {
    destroyAnimationEl();
    isStarted = false;
  }

  function createAnimationEl() {
    el = document.createElement('div');
    el.classList.add(styles.copyToClipboardNotification);
    el.innerText = 'link copied';
    rootEl.appendChild(el);
    el.addEventListener('transitionend', onTransitionEnd, false);
  }

  function destroyAnimationEl() {
    if (el === null) {
      return;
    }
    el.removeEventListener('transitionend', onTransitionEnd, false);
    rootEl.removeChild(el);
    el = null;
  }
}
