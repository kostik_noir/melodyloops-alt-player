import { h } from 'preact';
import BaseBtn from '../base-horizontal-btn';
import Tooltip from './tooltip';
import horizontalBtnStyles from '../styles/horizontal-btn.scss';
import styles from './styles.scss';

export default function RepeatBtn() {
  return (
    <div className={ styles.root }>
      <BaseBtn className={ horizontalBtnStyles.shareBtn } title={ 'share' }/>
      <div className={ styles.tooltip }>
        <Tooltip/>
      </div>
    </div>
  );
}
