import { h } from 'preact';
import { useState } from 'preact/hooks';
import BaseBtn from './base-horizontal-btn';
import * as styles from './styles/horizontal-btn.scss';

export default function ShareBtn() {
  const [isActive, changeState] = useState(false);
  const handleClick = () => {
    changeState(!isActive);
  };

  const title = isActive ? 'cycle off' : 'cycle on';

  return (
    <BaseBtn className={ styles.repeatBtn } title={ title } onClick={ handleClick }/>
  );
}
