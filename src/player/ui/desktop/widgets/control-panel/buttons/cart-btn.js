import { h } from 'preact';
import { connect } from 'react-redux';
import * as cart from '../../../../../state/cart';
import * as activeTrack from '../../../../../state/active-track';
import BaseBtn from './base-vertical-btn';
import * as styles from './styles/vertical-btn.scss';

const mapStateToProps = (state) => {
  const activeTrackId = activeTrack.getData(state).id;
  const isInCart = cart.isInCart(state, activeTrackId);
  return { isInCart };
};

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(cart.toggleActiveTrackState());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CartBtn);

function CartBtn({ onClick, isInCart }) {
  const handleClick = () => {
    onClick();
  };

  let className = [styles.cartBtn];
  if (isInCart) {
    className.push(styles.altState);
  }
  className = className.join(' ');

  const title = isInCart ? 'remove' : 'add to cart';

  return (
    <BaseBtn className={ className } title={ title } onClick={ handleClick }/>
  );
}
