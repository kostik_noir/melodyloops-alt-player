import { h } from 'preact';
import { connect } from 'react-redux';
import * as activeTrack from '../../../../../state/active-track';
import BaseBtn from './base-vertical-btn';
import * as styles from './styles/vertical-btn.scss';

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(activeTrack.selectNextTrack());
  }
});

export default connect(null, mapDispatchToProps)(NextBtn);

function NextBtn({ onClick }) {
  return (
    <BaseBtn className={ styles.nextBtn } title={ 'next' } onClick={ onClick }/>
  );
}
