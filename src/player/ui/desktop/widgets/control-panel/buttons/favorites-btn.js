import { h } from 'preact';
import { connect } from 'react-redux';
import { useState } from 'preact/hooks';
import * as activeTrack from '../../../../../state/active-track';
import * as favoritesDialog from '../../../../../../favorites-dialog';
import BaseBtn from './base-vertical-btn';
import * as styles from './styles/vertical-btn.scss';

const mapStateToProps = state => ({
  track: activeTrack.getData(state)
});

export default connect(mapStateToProps)(FavoritesBtn);

function FavoritesBtn({ track }) {
  const [isFavorite] = useState(false);
  const handleClick = () => {
    favoritesDialog.show({ trackId: track.id, title: track.title });
  };

  let className = [styles.favoritesBtn];
  if (isFavorite) {
    className.push(styles.altState);
  }
  className = className.join(' ');

  return (
    <BaseBtn className={ className } title={ 'favorites' } onClick={ handleClick }/>
  );
}
