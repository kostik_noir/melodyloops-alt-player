import { h } from 'preact';
import { useState } from 'preact/hooks';
import BaseBtn from './base-vertical-btn';
import * as styles from './styles/vertical-btn.scss';

export default function PlayBtn() {
  const [isPaused, changeState] = useState(false);
  const handleClick = () => {
    changeState(!isPaused);
  };

  let className = [styles.playBtn];
  if (isPaused) {
    className.push(styles.altState);
  }
  className = className.join(' ');

  const title = isPaused ? 'pause' : 'play';

  return (
    <BaseBtn className={ className } title={ title } onClick={ handleClick }/>
  );
}
