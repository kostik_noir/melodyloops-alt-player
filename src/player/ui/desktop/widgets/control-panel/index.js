import { h } from 'preact';
import PrevBtn from './buttons/prev-btn';
import PlayBtn from './buttons/play-btn';
import NextBtn from './buttons/next-btn';
import CartBtn from './buttons/cart-btn';
import FavoritesBtn from './buttons/favorites-btn';
import LongoloopsBtn from './buttons/longoloops-btn';
import RepeatBtn from './buttons/repeat-btn';
import ShareBtn from './buttons/share-btn';
import VolumeControl from './volume-control';
import * as styles from './styles.scss';

export default function ControlPanel({ className: extraClassName }) {
  let className = [styles.root];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  return (
    <div className={ className }>
      <div className={styles.topSection}>
        <div className={ styles.item }>
          <RepeatBtn/>
        </div>
        <div className={ styles.item }>
          <VolumeControl/>
        </div>
        <div className={ styles.item }>
          <ShareBtn/>
        </div>
      </div>
      <div className={ styles.bottomSection }>
        <div className={ styles.item }>
          <PrevBtn/>
        </div>
        <div className={ styles.item }>
          <PlayBtn/>
        </div>
        <div className={ styles.item }>
          <NextBtn/>
        </div>
        <div className={ styles.item }>
          <CartBtn/>
        </div>
        <div className={ styles.item }>
          <FavoritesBtn/>
        </div>
        <div className={ styles.item }>
          <LongoloopsBtn/>
        </div>
      </div>
    </div>
  );
}
