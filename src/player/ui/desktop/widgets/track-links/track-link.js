import { h } from 'preact';
import { connect } from 'react-redux';
import * as activeTrack from '../../../../state/active-track';
import * as urlBuilder from '../../../../utils/url-builder';
import * as styles from './styles.scss';

const mapStateToProps = (state) => {
  const { title, fileName } = activeTrack.getData(state);

  return {
    title,
    link: urlBuilder.getTrackPageUrl(fileName)
  };
};

export default connect(mapStateToProps)(TrackLink);

function TrackLink({ title, link, className: extraClassName }) {
  let className = [styles.root, styles.isTrackLink];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }

  className = className.join(' ');
  return (
    <a href={ link } className={ className }>
      <div className={ styles.title }>{ title }</div>
      <div className={ styles.icon }></div>
    </a>
  );
}
