import { h } from 'preact';
import { connect } from 'react-redux';
import * as activeTrack from '../../../../state/active-track';
import * as urlBuilder from '../../../../utils/url-builder';

import * as styles from './styles.scss';

const mapStateToProps = (state) => {
  const { composerName, composerLinkId } = activeTrack.getData(state);
  return {
    name: composerName,
    link: urlBuilder.getComposerPageUrl(composerLinkId)
  };
};

export default connect(mapStateToProps)(ComposerLink);

function ComposerLink({ name, link, className: extraClassName }) {
  let className = [styles.root, styles.isComposerLink];
  if (typeof extraClassName === 'string') {
    className.push(extraClassName);
  }
  className = className.join(' ');

  return (
    <a href={ link } className={ className }>
      <div className={ styles.title }>{ name }</div>
      <div className={ styles.icon }></div>
    </a>
  );
}
