import { h } from 'preact';
import DesktopUI from './desktop';

export default function PlayerUI() {
  return (
    <div>
      <DesktopUI/>
    </div>
  );
}
