import { h, render } from 'preact';
import { useEffect } from 'preact/hooks';
import { Provider } from 'react-redux';
import * as state from './state';
import Ui from './ui';
import { INIT_APP } from './common';

export function setup(node, options) {
  const store = state.create(options);

  const App = () => {
    useEffect(() => store.dispatch({ type: INIT_APP }), []);

    return (
      <Provider store={ store }>
        <Ui/>
      </Provider>
    );
  };

  render(<App/>, node);
}
