import { onDomReady } from './utils/dom';
import * as player from './player';
import * as markupParser from './utils/markup-parser';
import './common.scss';

onDomReady(() => {
  document.querySelectorAll('[data-ml-player-with-playlist]').forEach((node) => {
    const options = markupParser.parse(node);
    node.innerHTML = '';
    player.setup(node, options);
  });
});
