/* eslint-disable no-console */
export default () => next => (action) => {
  console.info('[log]', action);
  next(action);
};
