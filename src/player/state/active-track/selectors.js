import { MOUNT_POINT } from './constants';

export function getData(state) {
  return state[MOUNT_POINT];
}
