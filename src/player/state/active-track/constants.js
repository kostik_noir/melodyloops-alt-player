export const MOUNT_POINT = 'activeTrack';

export const REQUEST_TRACK_CHANGE = `${MOUNT_POINT}/requestTrackChanging`;
export const TRACK_CHANGED = `${MOUNT_POINT}/trackChanged`;
export const VARIATION_CHANGED = `${MOUNT_POINT}/variationChanged`;
export const SELECT_NEXT_TRACK = `${MOUNT_POINT}/selectNextTrack`;
export const SELECT_PREVIOUS_TRACK = `${MOUNT_POINT}/selectPreviousTrack`;

export const EMPTY_TRACK_ID = null;
