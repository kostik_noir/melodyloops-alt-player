import {
  REQUEST_TRACK_CHANGE,
  TRACK_CHANGED,
  VARIATION_CHANGED,
  SELECT_NEXT_TRACK,
  SELECT_PREVIOUS_TRACK
} from './constants';

export function changeTrack(track) {
  return {
    type: TRACK_CHANGED,
    payload: { track }
  };
}

export function requestTrackChange({ trackId, fromSelectedPlaylist, fromCartPlaylist }) {
  return {
    type: REQUEST_TRACK_CHANGE,
    payload: { trackId, fromSelectedPlaylist, fromCartPlaylist }
  };
}

export function changeVariation(index) {
  return {
    type: VARIATION_CHANGED,
    payload: {
      index
    }
  };
}

export function selectNextTrack() {
  return { type: SELECT_NEXT_TRACK };
}

export function selectPreviousTrack() {
  return { type: SELECT_PREVIOUS_TRACK };
}
