import * as localStorage from '../../utils/local-storage';
import { EMPTY_TRACK_ID } from './constants';

const localStorageKey = 'activeTracks';

export function createState(trackId, variations = []) {
  const selectedVariationIndex = findDefaultVariationIndex(variations);

  return {
    id: trackId,
    variations,
    selectedVariationIndex
  };
}

export function createEmptyState() {
  return createState(EMPTY_TRACK_ID);
}

export function findDefaultVariationIndex(variations) {
  const count = variations.length;
  for (let i = 0; i < count; i++) {
    if (variations[i].isDefault) {
      return i;
    }
  }
  return 0;
}

export function getStoredTrackIdForPlaylist(playlistUrl) {
  return getStoredData()[playlistUrl] || EMPTY_TRACK_ID;
}

export function storeActiveTrackIdForPlaylist(playlistUrl, trackId) {
  const data = getStoredData();
  data[playlistUrl] = trackId;

  try {
    localStorage.setItem(localStorageKey, JSON.stringify(data));
  } catch (e) {
  }
}

function getStoredData() {
  let data = localStorage.getItem(localStorageKey);
  try {
    data = JSON.parse(data);
  } catch (e) {
  }

  if (Object.prototype.toString.call(data) !== '[object Object]') {
    data = {};
  }

  return data;
}
