import {
  REQUEST_TRACK_CHANGE,
  TRACK_CHANGED,
  SELECT_NEXT_TRACK,
  SELECT_PREVIOUS_TRACK,
  EMPTY_TRACK_ID
} from './constants';
import {
  CHANGED as SELECTED_PLAYLIST_CHANGED,
  LOADING_COMPLETED as SELECTED_PLAYLIST_LOADING_COMPLETED
} from '../selected-playlist';
import * as selectedPlaylist from '../selected-playlist';
import * as utils from './utils';
import * as actionCreators from './action-creators';
import * as selectors from './selectors';
import * as cart from '../cart';

const extraDataKey = 'activeTrackExtraData';

export default store => next => (action) => {
  if (action.type === cart.TRACK_REMOVED) {
    action = modifyTrackRemovedFromCartAction(store, action);
  }
  next(action);

  switch (action.type) {
    case REQUEST_TRACK_CHANGE:
      onTrackChangeRequested(store, action);
      break;
    case TRACK_CHANGED:
      onActiveTrackChanged(store);
      break;
    case SELECTED_PLAYLIST_CHANGED:
      onSelectedPlaylistChanged(store);
      break;
    case SELECTED_PLAYLIST_LOADING_COMPLETED:
      onSelectedPlaylistLoaded(store);
      break;
    case SELECT_NEXT_TRACK:
      onNextTrackRequested(store);
      break;
    case SELECT_PREVIOUS_TRACK:
      onPreviousTrackRequested(store);
      break;
    case cart.TRACK_REMOVED:
      onTrackRemovedFromCart(store, action);
      break;
  }
};

function onSelectedPlaylistChanged(store) {
  store.dispatch(actionCreators.changeTrack({ id: EMPTY_TRACK_ID }));
}

function onSelectedPlaylistLoaded(store) {
  const state = store.getState();

  const playlistUrl = selectedPlaylist.getUrl(state);
  const storedTrackId = utils.getStoredTrackIdForPlaylist(playlistUrl);
  let track = selectedPlaylist.getTrackById(state, storedTrackId);
  if (track === null) {
    track = selectedPlaylist.getTracks(state)[0];
  }

  dispatchChangeTrack({ store, track, fromSelectedPlaylist: true });
}

function onTrackChangeRequested(store, action) {
  const { trackId, fromSelectedPlaylist, fromCartPlaylist } = action.payload;

  let track;
  switch (true) {
    case fromSelectedPlaylist:
      track = selectedPlaylist.getTrackById(store.getState(), trackId);
      break;
    case fromCartPlaylist:
      track = cart.getTrackById(store.getState(), trackId);
      break;
    default:
      track = null;
  }

  if (track === null) {
    return;
  }

  dispatchChangeTrack({ store, track, fromSelectedPlaylist, fromCartPlaylist });
}

function onActiveTrackChanged(store) {
  const state = store.getState();
  const { id } = selectors.getData(state);
  if (id === EMPTY_TRACK_ID) {
    return;
  }

  const playlistUrl = selectedPlaylist.getUrl(state);
  utils.storeActiveTrackIdForPlaylist(playlistUrl, id);
}

function onNextTrackRequested(store) {
  const state = store.getState();
  const currentActiveTrack = selectors.getData(state);
  const { fromSelectedPlaylist, fromCartPlaylist } = currentActiveTrack;
  const cartTracks = cart.getTracks(state);
  const selectedPlaylistTracks = selectedPlaylist.getTracks(state);

  let index;
  let track;
  let isCartPlaylist = false;
  let isSelectedPlaylist = false;

  switch (true) {
    case fromSelectedPlaylist:
      index = getTrackIndex(currentActiveTrack, selectedPlaylistTracks);
      if (Number.isNaN(index)) {
        index = 0;
      } else {
        index++;
      }

      if (index >= selectedPlaylistTracks.length) {
        index = 0;
      }

      track = selectedPlaylistTracks[index];
      isSelectedPlaylist = true;
      break;
    case fromCartPlaylist && cartTracks.length > 0:
      index = getTrackIndex(currentActiveTrack, cartTracks);
      if (Number.isNaN(index)) {
        index = 0;
      } else {
        index++;
      }

      if (index >= cartTracks.length) {
        index = 0;
      }

      track = cartTracks[index];
      isCartPlaylist = true;
      break;
    case fromCartPlaylist && cartTracks.length === 0:
      index = getTrackIndex(currentActiveTrack, selectedPlaylistTracks);
      if (Number.isNaN(index)) {
        index = 0;
      } else {
        index++;
      }

      if (index >= selectedPlaylistTracks.length) {
        index = 0;
      }

      track = selectedPlaylistTracks[index];
      isSelectedPlaylist = true;
      break;
    default:
      track = null;
  }

  if (track === null || typeof track === 'undefined') {
    return;
  }

  dispatchChangeTrack({
    store,
    track,
    fromSelectedPlaylist: isSelectedPlaylist,
    fromCartPlaylist: isCartPlaylist
  });
}

function onPreviousTrackRequested(store) {
  const state = store.getState();
  const currentActiveTrack = selectors.getData(state);
  const { fromSelectedPlaylist, fromCartPlaylist } = currentActiveTrack;
  const cartTracks = cart.getTracks(state);
  const selectedPlaylistTracks = selectedPlaylist.getTracks(state);

  let index;
  let track;
  let isCartPlaylist = false;
  let isSelectedPlaylist = false;

  switch (true) {
    case fromSelectedPlaylist:
      index = getTrackIndex(currentActiveTrack, selectedPlaylistTracks);
      if (Number.isNaN(index)) {
        index = 0;
      } else {
        index--;
      }

      if (index < 0) {
        index = selectedPlaylistTracks.length - 1;
      }

      track = selectedPlaylistTracks[index];
      isSelectedPlaylist = true;
      break;
    case fromCartPlaylist && cartTracks.length > 0:
      index = getTrackIndex(currentActiveTrack, cartTracks);
      if (Number.isNaN(index)) {
        index = 0;
      } else {
        index--;
      }

      if (index < 0) {
        index = cartTracks.length - 1;
      }

      track = cartTracks[index];
      isCartPlaylist = true;
      break;
    case fromCartPlaylist && cartTracks.length === 0:
      index = getTrackIndex(currentActiveTrack, selectedPlaylistTracks);
      if (Number.isNaN(index)) {
        index = 0;
      }

      track = selectedPlaylistTracks[index];
      isSelectedPlaylist = true;
      break;
    default:
      track = null;
  }

  if (track === null || typeof track === 'undefined') {
    return;
  }

  dispatchChangeTrack({
    store,
    track,
    fromSelectedPlaylist: isSelectedPlaylist,
    fromCartPlaylist: isCartPlaylist
  });
}

function getTrackIndex(track, tracks) {
  const count = tracks.length;
  for (let i = 0; i < count; i++) {
    if (tracks[i].id === track.id) {
      return i;
    }
  }

  return Math.NaN;
}

function onTrackRemovedFromCart(store, action) {
  const state = store.getState();

  const activeTrackData = selectors.getData(state);
  if (!activeTrackData.fromCartPlaylist) {
    return;
  }

  const { payload: { [extraDataKey]: { trackIndex: removedTrackIndex } } } = action;
  const cartTracks = cart.getTracks(state);
  if (cartTracks.length === 0) {
    return;
  }

  const index = Math.max(0, removedTrackIndex - 1);
  const track = cartTracks[index];

  dispatchChangeTrack({ store, track, fromCartPlaylist: true });
}

function dispatchChangeTrack({ store, track, fromSelectedPlaylist = false, fromCartPlaylist = false }) {
  const currentActiveTrack = selectors.getData(store.getState());

  const shouldSkip = currentActiveTrack.id === track.id
    && !!currentActiveTrack.fromCartPlaylist === !!fromCartPlaylist
    && !!currentActiveTrack.fromSelectedPlaylist === !!fromSelectedPlaylist;
  if (shouldSkip) {
    return;
  }

  store.dispatch(actionCreators.changeTrack({
    ...track,
    selectedVariationIndex: utils.findDefaultVariationIndex(track.variations),
    fromSelectedPlaylist,
    fromCartPlaylist
  }));
}

function modifyTrackRemovedFromCartAction(store, action) {
  const { payload: { trackId } } = action;
  const state = store.getState();
  const tracks = cart.getTracks(state);

  return {
    ...action,
    payload: {
      ...action.payload,
      [extraDataKey]: {
        trackIndex: getTrackIndex({ id: trackId }, tracks)
      }
    }
  };
}
