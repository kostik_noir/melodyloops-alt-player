import {
  MOUNT_POINT,
  TRACK_CHANGED,
  VARIATION_CHANGED
} from './constants';
import * as utils from './utils';

export function setup(map) {
  map[MOUNT_POINT] = reducer;

  const initialState = utils.createEmptyState();

  function reducer(state = initialState, action = {}) {
    switch (action.type) {
      case TRACK_CHANGED:
        return onTrackChanged(action);
      case VARIATION_CHANGED:
        return onVariationChanged(state, action);
      default:
        return state;
    }
  }
}

function onTrackChanged(action) {
  return action.payload.track;
}

function onVariationChanged(state, action) {
  return {
    ...state,
    selectedVariationIndex: action.payload.index
  };
}
