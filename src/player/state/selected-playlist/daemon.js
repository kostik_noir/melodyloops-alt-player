import { INIT_APP } from '../../common';
import { CHANGED, RELOAD } from './constants';
import * as selectors from './selectors';
import * as playlists from '../playlists';
import * as actionCreators from './action-creators';
import * as utils from './utils';

const activeLoaders = {};

export default store => next => (action) => {
  next(action);

  switch (action.type) {
    case INIT_APP:
      onAppInit(store);
      break;
    case CHANGED:
      onPlaylistChanged(store);
      break;
    case RELOAD:
      loadPlaylist(store);
      break;
  }
};

function onAppInit(store) {
  const allPlaylists = playlists.getPlaylists(store.getState());
  const playlistUrl = utils.getSelectedByDefaultPlaylistUrl(allPlaylists);
  store.dispatch(actionCreators.change(playlistUrl));
}

function onPlaylistChanged(store) {
  utils.storeSelectedPlaylist(selectors.getUrl(store.getState()));
  loadPlaylist(store);
}

function loadPlaylist(store) {
  const url = selectors.getUrl(store.getState());

  store.dispatch(actionCreators.loadingStarted());

  createLoader(url).load()
    .then((data) => {
      store.dispatch(actionCreators.loadingCompleted(data));
    })
    .catch((e) => {
      store.dispatch(actionCreators.loadingFailed(e));
    });
}

function createLoader(url) {
  if (typeof activeLoaders[url] !== 'undefined') {
    return activeLoaders[url];
  }

  activeLoaders[url] = {
    load
  };

  const unregisterLoader = () => {
    delete activeLoaders[url];
  };

  return activeLoaders[url];

  function load() {
    return fetch(url)
      .then(resp => resp.text())
      .then((data) => {
        unregisterLoader();
        return utils.parseServerResponse(data);
      })
      .catch((e) => {
        unregisterLoader();
        throw e;
      });
  }
}
