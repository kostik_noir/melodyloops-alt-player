export const MOUNT_POINT = 'selectedPlaylist';

export const CHANGED = `${MOUNT_POINT}/changed`;
export const LOADING_STARTED = `${MOUNT_POINT}/loadingStarted`;
export const LOADING_FAILED = `${MOUNT_POINT}/loadingFailed`;
export const LOADING_COMPLETED = `${MOUNT_POINT}/loadingCompleted`;
export const RELOAD = `${MOUNT_POINT}/reload`;

export const LOADING_STATE_NONE = 0;
export const LOADING_STATE_STARTED = 1;
export const LOADING_STATE_FAILED = 2;
export const LOADING_STATE_COMPLETED = 3;
