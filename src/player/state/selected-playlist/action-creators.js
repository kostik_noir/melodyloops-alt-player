import { CHANGED, LOADING_STARTED, LOADING_FAILED, LOADING_COMPLETED, RELOAD } from './constants';

export function change(playlistUrl) {
  return {
    type: CHANGED,
    payload: {
      playlistUrl
    }
  };
}

export function loadingStarted() {
  return {
    type: LOADING_STARTED
  };
}

export function loadingFailed(error) {
  return {
    type: LOADING_FAILED,
    payload: { error }
  };
}

export function loadingCompleted(playlistData) {
  return {
    type: LOADING_COMPLETED,
    payload: {
      playlistData
    }
  };
}

export function reload() {
  return {
    type: RELOAD
  };
}
