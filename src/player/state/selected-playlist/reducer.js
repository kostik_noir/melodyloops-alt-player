import {
  MOUNT_POINT,
  CHANGED,
  LOADING_COMPLETED,
  LOADING_FAILED,
  LOADING_STARTED,
  LOADING_STATE_NONE,
  LOADING_STATE_STARTED,
  LOADING_STATE_FAILED,
  LOADING_STATE_COMPLETED
} from './constants';

export function setup(map) {
  map[MOUNT_POINT] = reducer;

  const initialData = createInitialStateData('');

  function reducer(state = initialData, action = {}) {
    switch (action.type) {
      case CHANGED:
        return onChanged(action);
      case LOADING_STARTED:
        return onLoadingStarted(state);
      case LOADING_FAILED:
        return onLoadingFailed(state);
      case LOADING_COMPLETED:
        return onLoadingCompleted(state, action);
      default:
        return state;
    }
  }
}

function onChanged(action) {
  return createInitialStateData(action.payload.playlistUrl);
}

function onLoadingStarted(state) {
  return {
    ...state,
    loadingState: LOADING_STATE_STARTED
  };
}

function onLoadingFailed(state) {
  return {
    ...state,
    loadingState: LOADING_STATE_FAILED
  };
}

function onLoadingCompleted(state, action) {
  const { payload: { playlistData: { tracks } } } = action;
  const lookupMap = tracks.reduce((acc, obj) => {
    acc[obj.id] = obj;
    return acc;
  }, {});

  return {
    ...state,
    loadingState: LOADING_STATE_COMPLETED,
    tracks,
    lookupMap
  };
}

function createInitialStateData(playlistUrl) {
  return {
    url: playlistUrl,
    loadingState: LOADING_STATE_NONE,
    tracks: [],
    lookupMap: {}
  };
}
