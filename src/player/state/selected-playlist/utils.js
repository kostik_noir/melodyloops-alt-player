import * as localStorage from '../../utils/local-storage';

const storageKey = 'selectedPlaylistUrl';

export function getSelectedByDefaultPlaylistUrl(playlists = []) {
  const storedUrl = getStoredValue();
  if (playlists.some(obj => obj.url === storedUrl)) {
    return storedUrl;
  }

  const selectedByDefaultPlaylist = playlists.filter(obj => obj.isActive)[0];
  if (typeof selectedByDefaultPlaylist !== 'undefined') {
    return selectedByDefaultPlaylist.url;
  }

  if (playlists.length > 0) {
    return playlists[0].url;
  }

  return '';
}

function getStoredValue() {
  return localStorage.getItem(storageKey);
}

export function parseServerResponse(xmlStr) {
  const parser = new DOMParser();
  const dom = parser.parseFromString(xmlStr, 'application/xml');

  return {
    tracks: Array.from(dom.querySelectorAll('track')).map(parseTrackXmlNode)
  };
}

function parseTrackXmlNode(node) {
  const id = node.getAttribute('id');
  const defaultVariationDuration = parseInt(node.getAttribute('duration'), 10);

  const url = node.getAttribute('url');
  const urlParseResult = /(.+)\/([^./]+)\.[^.]+$/gi.exec(url);
  const [, baseUrl, rawFileName] = urlParseResult;
  const fileName = rawFileName.replace(/^preview-/gi, '');

  const title = node.getAttribute('title');
  const composerName = node.getAttribute('cname');
  const composerLinkId = node.getAttribute('curl');

  const extraVariations = [];
  node.getAttribute('ext').split('-').forEach((v) => {
    const regExpResult = /^u([0-9]*)$/gi.exec(v);

    let isUnlooped = false;
    let duration;
    if (Array.isArray(regExpResult)) {
      duration = parseInt(regExpResult[1], 10);
      isUnlooped = true;
    } else {
      duration = parseInt(v, 10);
    }

    if (Number.isNaN(duration)) {
      return;
    }

    extraVariations.push({
      duration,
      isUnlooped
    });
  });

  const variations = [].concat(extraVariations, { duration: defaultVariationDuration, isDefault: true });

  return {
    id,
    title,
    baseUrl,
    fileName,
    variations,
    composerName,
    composerLinkId
  };
}

export function storeSelectedPlaylist(playlistUrl) {
  try {
    localStorage.setItem(storageKey, playlistUrl);
  } catch (e) {
  }
}
