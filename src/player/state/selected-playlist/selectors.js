import {
  MOUNT_POINT,
  LOADING_STATE_COMPLETED,
  LOADING_STATE_FAILED,
  LOADING_STATE_STARTED
} from './constants';

export function getUrl(state) {
  return state[MOUNT_POINT].url;
}

export function getTracks(state) {
  return state[MOUNT_POINT].tracks;
}

export function isLoadingStarted(state) {
  return state[MOUNT_POINT].loadingState === LOADING_STATE_STARTED;
}

export function isLoadingFailed(state) {
  return state[MOUNT_POINT].loadingState === LOADING_STATE_FAILED;
}

export function isLoadingCompleted(state) {
  return state[MOUNT_POINT].loadingState === LOADING_STATE_COMPLETED;
}

export function getTrackById(state, trackId) {
  return state[MOUNT_POINT].lookupMap[trackId] || null;
}
