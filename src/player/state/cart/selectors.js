import { MOUNT_POINT } from './constants';

export function getTracks(state) {
  return state[MOUNT_POINT].tracks;
}

export function getTrackById(state, trackId) {
  return state[MOUNT_POINT].lookupMap[trackId];
}

export function isInCart(state, trackId) {
  return typeof getTrackById(state, trackId) !== 'undefined';
}
