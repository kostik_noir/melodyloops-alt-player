import * as activeTrack from '../active-track';
import * as selectedPlaylist from '../selected-playlist';
import { TRACK_ADDED, TRACK_REMOVED, TOGGLE_ACTIVE_TRACK_STATE } from './constants';
import * as actionCreators from './action-creators';
import * as selectors from './selectors';
import * as utils from './utils';

export default store => next => (action) => {
  if (action.type === TRACK_ADDED) {
    action = modifyAddTrackAction(store, action);
  }
  next(action);

  switch (action.type) {
    case TOGGLE_ACTIVE_TRACK_STATE:
      handleToggleTrackState(store, action);
      break;
    case TRACK_ADDED:
    case TRACK_REMOVED:
      saveChangesInLocalStorage(store);
      break;
  }
};

function handleToggleTrackState(store) {
  const state = store.getState();
  const track = activeTrack.getData(state);
  if (selectors.isInCart(state, track.id)) {
    store.dispatch(actionCreators.removeTrack(track.id));
  } else {
    store.dispatch(actionCreators.addTrack(track.id));
  }
}

function saveChangesInLocalStorage(store) {
  const tracks = selectors.getTracks(store.getState());
  utils.saveData(tracks);
}

function modifyAddTrackAction(store, action) {
  return {
    ...action,
    payload: {
      track: selectedPlaylist.getTrackById(store.getState(), action.payload.trackId)
    }
  };
}
