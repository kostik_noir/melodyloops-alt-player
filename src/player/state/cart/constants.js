export const MOUNT_POINT = 'cart';

export const TRACK_ADDED = `${MOUNT_POINT}/trackAdded`;
export const TRACK_REMOVED = `${MOUNT_POINT}/trackRemoved`;
export const TOGGLE_ACTIVE_TRACK_STATE = `${MOUNT_POINT}/toggleActiveTrackState`;
