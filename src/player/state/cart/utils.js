import * as localStorage from '../../utils/local-storage';

const localStorageKey = 'cartTracks';

export function restoreData() {
  let data;
  try {
    data = JSON.parse(localStorage.getItem(localStorageKey));
  } catch (e) {
    data = [];
  }
  return Array.isArray(data) ? data : [];
}

export function saveData(tracks) {
  try {
    localStorage.setItem(localStorageKey, JSON.stringify(tracks));
  } catch (e) {
  }
}
