import { MOUNT_POINT, TRACK_ADDED, TRACK_REMOVED } from './constants';
import * as utils from './utils';

export function setup(map) {
  map[MOUNT_POINT] = reducer;

  const tracks = utils.restoreData();
  const lookupMap = tracks.reduce((acc, obj) => {
    acc[obj.id] = obj;
    return acc;
  }, {});

  const initialState = { tracks, lookupMap };

  function reducer(state = initialState, action = {}) {
    switch (action.type) {
      case TRACK_ADDED:
        return handleAddTrack(state, action);
      case TRACK_REMOVED:
        return handleRemoveTrack(state, action);
      default:
        return state;
    }
  }
}

function handleAddTrack(state, action) {
  const { payload: { track } } = action;

  const tracks = [...state.tracks, track];
  const lookupMap = { ...state.lookupMap, [track.id]: track };

  return { tracks, lookupMap };
}

function handleRemoveTrack(state, action) {
  const { payload: { trackId } } = action;

  const tracks = state.tracks.filter(obj => obj.id !== trackId);
  const lookupMap = { ...state.lookupMap };
  delete lookupMap[trackId];

  return { tracks, lookupMap };
}
