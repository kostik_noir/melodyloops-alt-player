import { TRACK_ADDED, TRACK_REMOVED, TOGGLE_ACTIVE_TRACK_STATE } from './constants';

export function addTrack(trackId) {
  return {
    type: TRACK_ADDED,
    payload: { trackId }
  };
}

export function removeTrack(trackId) {
  return {
    type: TRACK_REMOVED,
    payload: { trackId }
  };
}

export function toggleActiveTrackState() {
  return {
    type: TOGGLE_ACTIVE_TRACK_STATE
  };
}
