import { MOUNT_POINT } from './constants';

export function getPlaylists(state) {
  return state[MOUNT_POINT];
}
