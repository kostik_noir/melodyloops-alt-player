import { MOUNT_POINT } from './constants';

export function setup(map, playlists) {
  map[MOUNT_POINT] = reducer;

  const initialState = initialDataToInitialState(playlists);

  function reducer(state = initialState) {
    return state;
  }
}

function initialDataToInitialState(playlists = []) {
  return playlists.map((obj) => {
    const {
      url,
      title
    } = obj;

    return {
      url,
      title
    };
  });
}
