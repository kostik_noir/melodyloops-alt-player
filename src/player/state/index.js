import { createStore, combineReducers, applyMiddleware } from 'redux';
import * as cart from './cart';
import * as playlists from './playlists';
import * as selectedPlaylist from './selected-playlist';
import * as activeTrack from './active-track';
import logger from './logger';

export function create(initialData = {}) {
  const reducers = {};
  let middlewares = [];

  if (process.env.NODE_ENV === 'development') {
    middlewares.push(logger);
  }

  cart.setupReducer(reducers);
  middlewares = middlewares.concat(cart.middleware);

  playlists.setupReducer(reducers, initialData.playlists);

  selectedPlaylist.setupReducer(reducers);
  middlewares = middlewares.concat(selectedPlaylist.middleware);

  activeTrack.setupReducer(reducers);
  middlewares = middlewares.concat(activeTrack.middleware);

  return createStore(combineReducers(reducers), applyMiddleware(...middlewares));
}
