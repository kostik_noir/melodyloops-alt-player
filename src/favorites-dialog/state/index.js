import { createStore, combineReducers, applyMiddleware } from 'redux';
import * as track from './track';
import * as playlists from './playlists';
import logger from '../../player/state/logger';

export function create({ trackId, title }) {
  const reducers = {};
  let middlewares = [];

  if (process.env.NODE_ENV === 'development') {
    middlewares.push(logger);
  }

  track.setupReducer(reducers, { trackId, title });

  playlists.setupReducer(reducers);
  middlewares = middlewares.concat(playlists.middleware);

  return createStore(combineReducers(reducers), applyMiddleware(...middlewares));
}
