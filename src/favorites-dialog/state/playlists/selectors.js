import { MOUNT_POINT } from './constants';

export function isLoadingStarted(state) {
  return state[MOUNT_POINT].isLoadingStarted;
}

export function isLoadingFailed(state) {
  return state[MOUNT_POINT].isLoadingFailed;
}

export function isLoadingCompleted(state) {
  return state[MOUNT_POINT].isLoadingCompleted;
}

export function getPlaylists(state) {
  return state[MOUNT_POINT].data;
}
