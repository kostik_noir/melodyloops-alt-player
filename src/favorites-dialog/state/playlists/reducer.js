import { MOUNT_POINT, LOADING_STARTED, LOADING_FAILED, LOADING_COMPLETED } from './constants';

export function setup(map) {
  map[MOUNT_POINT] = reducer;

  const initialState = {
    isLoadingStarted: false,
    isLoadingFailed: false,
    isLoadingCompleted: false,
    data: []
  };

  function reducer(state = initialState, action = {}) {
    switch (action.type) {
      case LOADING_STARTED:
        return onLoadingStarted();
      case LOADING_FAILED:
        return onLoadingFailed();
      case LOADING_COMPLETED:
        return onLoadingCompleted(action);
      default:
        return state;
    }
  }

  function onLoadingStarted() {
    return {
      isLoadingStarted: true,
      isLoadingFailed: false,
      isLoadingCompleted: false,
      data: []
    };
  }

  function onLoadingFailed() {
    return {
      isLoadingStarted: false,
      isLoadingFailed: true,
      isLoadingCompleted: false,
      data: []
    };
  }

  function onLoadingCompleted(action) {
    return {
      isLoadingStarted: false,
      isLoadingFailed: false,
      isLoadingCompleted: true,
      data: action.payload
    };
  }
}
