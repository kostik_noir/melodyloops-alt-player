import { LOADING_REQUESTED } from './constants';
import * as actionCreators from './action-creators';
import * as selectors from './selectors';

export default store => next => action => {
  next(action);

  switch (action.type) {
    case LOADING_REQUESTED:
      load();
      break;
  }

  function load() {
    if (selectors.isLoadingStarted(store.getState())) {
      return;
    }
    store.dispatch(actionCreators.loadingStarted());

    fetch(getApiUrl())
      .then(resp => resp.json())
      .then((data) => {
        store.dispatch(actionCreators.loadingCompleted(data));
      })
      .catch((err) => {
        store.dispatch(actionCreators.loadingFailed(err));
      });
  }
};

function getApiUrl() {
  let url = `${process.env.API_SERVER_ROOT_URL}/favorites`;
  if(!/^http?s\/\/:/gi.test(url)) {
    return `http://${url}`;
  }
  return url;
}
