import { LOADING_STARTED, LOADING_FAILED, LOADING_COMPLETED, LOADING_REQUESTED } from './constants';

export function loadingStarted() {
  return { type: LOADING_STARTED };
}

export function loadingFailed(err) {
  return { type: LOADING_FAILED, payload: err };
}

export function loadingCompleted(data) {
  return { type: LOADING_COMPLETED, payload: data };
}

export function load() {
  return { type: LOADING_REQUESTED };
}
