export const MOUNT_POINT = 'favoritePlaylists';

export const LOADING_REQUESTED = `${MOUNT_POINT}/loadingRequested`;
export const LOADING_STARTED = `${MOUNT_POINT}/loadingStarted`;
export const LOADING_FAILED = `${MOUNT_POINT}/loadingFailed`;
export const LOADING_COMPLETED = `${MOUNT_POINT}/loadingCompleted`;
