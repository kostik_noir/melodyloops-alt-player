import { MOUNT_POINT } from './constants';

export function setup(map, { id, title }) {
  map[MOUNT_POINT] = reducer;

  function reducer() {
    return { id, title };
  }
}
