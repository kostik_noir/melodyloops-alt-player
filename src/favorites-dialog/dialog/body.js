import { h } from 'preact';
import { connect } from 'react-redux';
import * as playlists from '../state/playlists';
import styles from './styles.scss';
import ScreenLoading from "../ui/screen-loading";
import ScreenError from "../ui/screen-error";
import ScreenReady from "../ui/screen-ready";

const mapStateToProps = state => ({
  isLoadingStarted: playlists.isLoadingStarted(state),
  isLoadingFailed: playlists.isLoadingFailed(state),
  isLoadingCompleted: playlists.isLoadingCompleted(state)
});

export default connect(mapStateToProps)(FavoritesDialogBody);

function FavoritesDialogBody({ isLoadingStarted, isLoadingFailed, isLoadingCompleted }) {
  let stateUi;
  switch (true) {
    case isLoadingStarted:
      stateUi = (<ScreenLoading/>);
      break;
    case isLoadingFailed:
      stateUi = (<ScreenError/>);
      break;
    case isLoadingCompleted:
      stateUi = (<ScreenReady/>);
      break;
    default:
      stateUi = null;
  }

  return <div className={ styles.body }>{ stateUi }</div>;
}
