import { h } from 'preact';
import { connect } from 'react-redux';
import * as track from '../state/track';
import styles from './styles.scss';

const mapStateToProps = state => ({
  trackTitle: track.getData(state).title
});

export default connect(mapStateToProps)(FavoritesDialog);

function FavoritesDialog({ trackTitle, onCancel }) {
  return (
    <div className={ styles.header } onClick={ onCancel }>
      <div className={ styles.closeBtn }></div>
      <div className={ styles.title }>Save to Favorites</div>
      <div className={ styles.subTitle }>{ trackTitle }</div>
    </div>
  );
}
