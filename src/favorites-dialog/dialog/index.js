import { h, Fragment } from 'preact';
import { useEffect } from 'preact/hooks';
import { connect } from 'react-redux';
import * as playlists from '../state/playlists';
import Header from './header';
import Body from './body';
import styles from './styles.scss';

const mapDispatchToProps = dispatch => ({
  load: () => {
    dispatch(playlists.load())
  }
});

export default connect(null, mapDispatchToProps)(FavoritesDialog);

function FavoritesDialog({ load, onCancel }) {
  const tryCallOnCancel = () => {
    if (typeof onCancel === 'function') {
      onCancel();
    }
  };

  useEffect(() => {
    const onKeyUp = (event) => {
      if (event.key !== 'Escape') {
        return;
      }
      tryCallOnCancel();
    };

    document.addEventListener('keyup', onKeyUp, false);
    load();

    return () => {
      document.removeEventListener('keyup', onKeyUp, false);
    };
  }, []);

  return (
    <Fragment>
      <div className={ styles.bg } onClick={ tryCallOnCancel }></div>
      <div className={ styles.dialog }>
        <Header onCancel={ tryCallOnCancel }/>
        <Body/>
      </div>
    </Fragment>
  );
}
