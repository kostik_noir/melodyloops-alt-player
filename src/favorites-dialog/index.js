import { h, render } from 'preact';
import { Provider } from 'react-redux';
import Dialog from './dialog';
import * as state from './state';
import styles from './styles.scss';

let isActive = false;

export function show({ trackId, title, onCancel }) {
  if (isActive) {
    return;
  }
  isActive = true;

  const root = document.createElement('div');
  root.classList.add(styles.root);
  document.body.appendChild(root);

  const store = state.create({ trackId, title });

  let dialog;

  const destroy = () => {
    render(null, root, dialog);
    document.body.removeChild(root);
    isActive = false;
  };

  const handleOnCancel = () => {
    destroy();
    if (typeof onCancel === 'function') {
      onCancel();
    }
  };

  dialog = render((
    <Provider store={ store }>
      <Dialog onCancel={ handleOnCancel }/>
    </Provider>
  ), root);
}
