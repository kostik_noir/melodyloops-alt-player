import { h } from 'preact';
import Spinner from '../../../common/ui/spinner';
import styles from './styles.scss';

export default function StateLoading() {
  return (
    <div className={ styles.root }>
      <Spinner/>
    </div>
  );
}
