import { h } from 'preact';
import { connect } from 'react-redux';
import * as playlists from '../../state/playlists';
import styles from './styles.scss';

const mapDispachToProps = dispatch => ({
  onTryAgainBtnPressed: () => {
    dispatch(playlists.load());
  }
});

export default connect(null, mapDispachToProps)(StateError);

function StateError({ onTryAgainBtnPressed }) {
  return (
    <div className={ styles.root }>
      <div className={ styles.msg }>
        <div className={ styles.icon }></div>
        <div className={ styles.title }>Can't load data</div>
        <div className={ styles.btn } onClick={ onTryAgainBtnPressed }>Try again</div>
      </div>
    </div>
  );
}
