import { h, Fragment } from 'preact';
import SearchField from './search-field';
import List from './list';
import styles from './styles.scss';

export default function StateReady({ onCancel }) {
  return (
    <div className={ styles.root }>
      <SearchField/>
      <List/>
    </div>
  );
}
