import { h } from 'preact';
import styles from './styles.scss';

export default function () {
  return (
    <div className={ styles.root }>
      <div className={ styles.inputWrapper }>
        <input type="text" className={ styles.input }/>
        <div className={ styles.placeholder }>Create new playlist or search</div>
        <div className={ styles.clearBtn }></div>
        <div className={ styles.focusBorder }></div>
      </div>
      <div className={ styles.submitBtn }>save</div>
    </div>
  );
}
