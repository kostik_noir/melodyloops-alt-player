import { h } from 'preact';
import * as styles from './styles.scss';

export default function Spinner() {
  return (
    <div className={ styles.root }></div>
  );
}
