const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { sourceRootDir, isProductionMode } = require('../../project-config');

const htmlMinifierCfg = {
  collapseBooleanAttributes: true,
  collapseInlineTagWhitespace: true,
  collapseWhitespace: true,
  removeComments: true,
  removeScriptTypeAttributes: true
};

module.exports = (webpackCfg) => {
  webpackCfg.module.rules = [
    ...webpackCfg.module.rules,
    {
      test: /\.twig$/,
      use: [
        'raw-loader',
        'twig-html-loader'
      ]
    }
  ];

  const entries = [
    {
      filename: 'index.html',
      template: path.resolve(sourceRootDir, 'demo/index.twig')
    }
  ];

  entries.forEach((entry) => {
    entry.minify = isProductionMode ? htmlMinifierCfg : false;
    webpackCfg.plugins.push(new HtmlWebpackPlugin(entry));
  });
};
