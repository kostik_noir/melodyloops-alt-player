module.exports = (webpackCfg) => {
  webpackCfg.resolve.alias.react = 'preact/compat';
  webpackCfg.resolve.alias['react-dom'] = 'preact/compat';
};
