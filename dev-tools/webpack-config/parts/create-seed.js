const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { isProductionMode, projectRootDir } = require('../../project-config');

const { API_SERVER_HOST, API_SERVER_PORT, API_SERVER_ROOT } = process.env;
const apiServerRoot = `${API_SERVER_HOST}:${API_SERVER_PORT}/${API_SERVER_ROOT}`;

module.exports = () => ({
  context: projectRootDir,
  target: 'web',
  mode: isProductionMode ? 'production' : 'development',
  entry: null,
  output: null,
  module: {
    rules: [],
    noParse: []
  },
  resolve: {
    alias: {}
  },
  performance: {},
  optimization: {
    minimize: isProductionMode,
    minimizer: []
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        API_SERVER_ROOT_URL: JSON.stringify(apiServerRoot)
      }
    }),
    new CleanWebpackPlugin()
  ]
});
