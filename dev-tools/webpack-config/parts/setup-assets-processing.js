const { isProductionMode } = require('../../project-config');

const assetNamePattern = 'assets/[name]_[hash].[ext]';

const imagesRule = {
  test: /\.(gif|png|jpe?g|ico)$/,
  use: [
    {
      loader: 'url-loader',
      options: {
        limit: 1000,
        name: assetNamePattern
      }
    }
  ]
};
if (isProductionMode) {
  imagesRule.use.unshift({
    loader: 'image-webpack-loader'
  });
}

const fontsRule = {
  test: /\.(eot|svg|ttf|woff|woff2)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: assetNamePattern
      }
    }
  ]
};

module.exports = (webpackCfg) => {
  webpackCfg.module.rules.push(imagesRule);
  webpackCfg.module.rules.push(fontsRule);
};
