const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { isProductionMode, sourceRootDir } = require('../../project-config');

const miniCssExtractPluginLoaderCfg = {
  loader: MiniCssExtractPlugin.loader
};

const styleLoaderCfg = {
  loader: 'style-loader'
};

const cssLoaderCfg = {
  loader: 'css-loader',
  options: {
    sourceMap: !isProductionMode,
    importLoaders: 3,
    localsConvention: 'camelCaseOnly',
    modules: {
      mode: 'local',
      localIdentName: isProductionMode ? '[hash]' : '[path][name]__[local]--[hash:base64:5]',
      context: sourceRootDir
    }
  }
};

const resolveUrlLoaderCfg = {
  loader: 'resolve-url-loader',
  options: {
    sourceMap: !isProductionMode
  }
};

const postCssLoaderCfg = {
  loader: 'postcss-loader'
};

const sassLoaderCfg = {
  loader: 'sass-loader',
  options: {
    sourceMap: true // it's required for resolve-url-loader
  }
};

module.exports = (webpackConfig) => {
  webpackConfig.module.rules.push({
    test: /\.css$/,
    use: [
      isProductionMode ? miniCssExtractPluginLoaderCfg : styleLoaderCfg,
      cssLoaderCfg,
      postCssLoaderCfg,
      resolveUrlLoaderCfg
    ]
  });

  webpackConfig.module.rules.push({
    test: /\.(scss|sass)$/,
    use: [
      isProductionMode ? miniCssExtractPluginLoaderCfg : styleLoaderCfg,
      cssLoaderCfg,
      postCssLoaderCfg,
      resolveUrlLoaderCfg,
      sassLoaderCfg
    ]
  });

  if (isProductionMode) {
    webpackConfig.plugins.push(new MiniCssExtractPlugin({
      filename: '[name]-[chunkhash].css'
    }));
  }
};
