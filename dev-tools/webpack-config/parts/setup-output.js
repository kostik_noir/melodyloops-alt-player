const { distRootDir, publicPath, isProductionMode } = require('../../project-config');

module.exports = (webpackCfg) => {
  webpackCfg.output = {
    path: distRootDir,
    publicPath,
    filename: isProductionMode ? '[name]-[chunkhash].js' : '[name].js',
    chunkFilename: isProductionMode ? '[name]-[chunkhash].chunk.js' : '[name].chunk.js'
  };
};
