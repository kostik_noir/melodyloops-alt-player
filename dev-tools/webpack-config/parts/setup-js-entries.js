const path = require('path');
const { sourceRootDir } = require('../../project-config');

const entries = {
  demo: path.resolve(sourceRootDir, 'demo/index.js'),
  'player-with-playlist': path.resolve(sourceRootDir, 'player/index.js')
};

module.exports = (webpackConfig) => {
  webpackConfig.entry = entries;
};
