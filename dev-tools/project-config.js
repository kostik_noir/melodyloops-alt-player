const path = require('path');

const projectRootDir = path.resolve(__dirname, '..');
module.exports.projectRootDir = projectRootDir;

module.exports.sourceRootDir = path.resolve(projectRootDir, 'src');

module.exports.distRootDir = path.resolve(projectRootDir, 'dist');

module.exports.publicPath = '/';

module.exports.isProductionMode = process.env.NODE_ENV === 'production';
